---
title: "My Garden"
---

I'm Saschenka, and I'll use this website to create and reflect, whether it's writing poems like this:

> This place is a garden for me,  
> a place where ideas and understandng can grow,  
> from mundane experiences and words I sow,  
> into a fecund and nourishing bounty.

or remembering passages like the one below, from Alexander Berkman's [*Prison Memiors of an Anarchist*:](https://theanarchistlibrary.org/library/alexander-berkman-prison-memoirs-of-an-anarchist.pdf)

> With a glow of pleasure, I become aware of the note of tenderness in his voice. 
> Presently he surprises me by asking:  
> “Friend Aleck, what do they call you in Russian?"  
> He prefers the fond “Sashenka,” enunciating the strange word with quaint endearment, then diffidently confesses dislike for his own name, and relates the story he had recently read of apoor castaway Cuban youth; Filipe was his name, and he was just like himself.  
> “Shall I call you Filipe?” I offer.  
> “Please do, Aleck, dear; no, Sashenka.”  
> The springs of affection well up within me, as I lie huddled on the stone floor, cold and hungry.
> With closed eyes, I picture the boy before me, with his delicate face, and sensitive lips.  
> “Good night, dear Sashenka,” he calls.  
> “Good night, little Felipe.”  

I'll also write stories of experiences I've had, write articles explaining ideas I've been thinking about, and discuss things I read.
This site will basically be a diary, focusing on the (often overlapping) parts of my life involving radical politics, mental health, and queerness.
