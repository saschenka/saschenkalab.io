---
title: "Desire"
date: 2020-07-24
categories: ["poetry"]
tags: ["utopianism"]
type: poetry
---

As long as there are duties, our greatest duty to ourselves and each other  
is to make duty redundant; let it whither,  
as the complexity and capacity of our desires grow.  
What greater satisfaction could we know,  
than a world of wills where every action or passivity is a choice,  
a world with so many possibilities that there are no necessities,  
a world without duty but with doubtless luxury,  
a world of desire.
